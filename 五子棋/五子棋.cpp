#include <graphics.h>//eaxyx图形库
#include <stdio.h>
#include <math.h>
#include <mmsystem.h>//实现背景音乐的库
#include <vector>
#include <Windows.h>

#pragma comment(lib,"winmm.lib")//实现背景音乐的库

using namespace std;

void welcome();
void background();
void music();
void board_line();
void newgame_all();
void newgame_regret();
void playChess();
void button();
int defeat_black();
int defeat_white();
int judge(int a, int b);


typedef struct undo{
	vector<int> x;
	vector<int> y;
};
undo board_info;//使用容器记录棋子坐标，间接记录棋子颜色和棋子下的顺序
int flag = 0;				//标记下棋步数，间接标记轮到谁下棋
int board_now[16][16] = { 0 };//记录当前棋盘状态，0无棋子，1黑，2白

int main()
{
	initgraph(800, 640);						//窗口大小
	welcome();
	background();
	music();
	board_line();
	button();
	playChess();
	return 0;
}
void welcome() {
	//设置图像的背景颜色
	loadimage(NULL,"加载欢迎界面.jpg");
	Sleep(3000);
	// 应用到图像中
}
void background(){
	setbkcolor(RGB(255, 200, 88));//设置背景颜色
	cleardevice();	
}
void music() {
	mciSendString("open 追梦赤子心.mp3", 0, 0, 0);	//打开音乐
	mciSendString("play 追梦赤子心.mp3", 0, 0, 0);	//播放音乐
}
void board_line(){
	setlinecolor(BLACK);//设置线条颜色为黑色
	setlinestyle(PS_SOLID, 2);//设置线条样式
	for (int i = 40; i < 640; i += 40) {//构造棋盘
		line(i, 40, i, 600);//竖向划线构造棋盘(15根线）
		line(40, i, 600, i);//横向划线构造棋盘（15根线,16根的位置为边界）
	}
}

/*设置按钮文字样式和按钮*/
void button() {
	setlinestyle(PS_SOLID, 2);
	setbkmode(TRANSPARENT);
	settextcolor(BROWN);
	settextstyle(50, 0, _T("微软雅黑"));
	outtextxy(630, 50, _T("悔棋"));
	outtextxy(630, 130, _T("黑棋认输"));
	outtextxy(630, 200, _T("白棋认输"));
	outtextxy(630, 270, _T("重新开始"));
	//outtextxy(630, 340, _T("历史对局"));
	setlinecolor(YELLOW);
	rectangle(630, 55, 710, 100);
	rectangle(630, 135, 790, 180);
	rectangle(630, 206, 790, 250);
	rectangle(630, 270, 790, 320);
	//rectangle(630, 340, 790, 390);
}

/*归零游戏数据，重新开始*/
void newgame_data() {
	board_info.x.clear();
	board_info.y.clear();
	flag = 0;
	for (int i = 1; i < 16; i++) {
		for (int j = 0; j < 16; j++) {
			board_now[i][j] = 0;
		}
	}
}

/*归零游戏数据，重新绘制棋盘，棋子清零，重新开始*/
void newgame_all() {
	newgame_data();
	background();
	music();
	board_line();
	button();
}

/*重新绘制棋盘*/
void newgame_regret() {
	background();
	board_line();
	button();
}
void playChess()
{
	
	int x = 0, y = 0;
	int a = 0, b = 0;
	int i, j;
	MOUSEMSG m;		//定义鼠标消息
	while (1)		//实现不断下棋
	{
		m = GetMouseMsg();			//获取一个鼠标消息
		//求距离绝对值

		if (m.uMsg == WM_LBUTTONDOWN) {//分析鼠标消息，根据鼠标点击位置执行相应指令

			/*校准鼠标点击位置，画圆实现棋子落下，根据judge函数的返回值判断是否游戏结束*/
			if (m.x >= 0 && m.x <= 620 && m.y >= 0 && m.y <= 640) {//下棋
				for (i = 1; i < 16; i++) {
					for (j = 1; j < 16; j++) {
						if (abs(m.x - i * 40) < 20 && abs(m.y - j * 40) < 20) {
							a = i;
							b = j;
							x = i * 40;
							y = j * 40;
						}
					}
				}
				if (board_now[a][b] != 0) {
					MessageBox(NULL, "这里已经有棋子了,请重新选择.", "五子棋", MB_OK);
					continue;
				}
				if (flag % 2 == 0) {
					setfillcolor(BLACK);
					solidcircle(x, y, 20);
					board_info.x.push_back(a);
					board_info.y.push_back(b);
					board_now[a][b] = 1;
					flag++;
				}
				else {
					setfillcolor(WHITE);
					solidcircle(x, y, 20);
					board_info.x.push_back(a);
					board_info.y.push_back(b);
					board_now[a][b] = 2;
					flag++;
				}
				if (judge(a, b)) {
					if (flag == 225) {
						MessageBox(NULL, "和棋", "游戏结束", MB_OK);
						continue;
					}
					if (flag % 2 == 1) {
						MessageBox(NULL, "恭喜黑棋胜利", "游戏结束", MB_OK);
						newgame_all();
						continue;
					}
					else {
						MessageBox(NULL, "恭喜白棋胜利", "游戏结束", MB_OK);
						newgame_all();
						continue;
					}
				}
			}

			/*从容器中取出上一个棋子标号坐标，删除一个元素，重新加载回来少一个棋子的棋盘*/
			else if (m.x >= 630 && m.x <= 710 && m.y >= 55 && m.y <= 100) {//悔棋
				if (board_info.x.size() > 0 && board_info.y.size() > 0) {//有棋子记录，才可以悔棋
					a = board_info.x[board_info.x.size() - 1];
					b = board_info.y[board_info.y.size() - 1];
					board_info.x.pop_back();
					board_info.y.pop_back();
					board_now[a][b] = 0;
					flag--;
					newgame_regret();
					for (int i = 1; i < 16; i++) {	// 将地图上的棋子摆回来
						for (int j = 1; j < 16; j++) {
							if (board_now[i][j] == 1) {
								setfillcolor(BLACK);
								solidcircle(40 * i,40 * j, 20);
							}
							else if (board_now[i][j] == 2) {
								setfillcolor(WHITE);
								solidcircle(40 * i,40 * j, 20);
							}
						}
					}
				}
				else {
					MessageBox(NULL, "无记录，无法悔棋", "请重新操作", MB_OK);
				}
			}
			else if (m.x >= 630 && m.x <= 790 && m.y >= 135 && m.y <= 180) {//黑棋认输
				defeat_black();
			}
			else if (m.x >= 630 && m.x <= 790 && m.y >= 206 && m.y <= 250) {//白棋认输
				defeat_white();
			}
			else if (m.x >= 630 && m.x <= 790 && m.y >= 270 && m.y <= 320) {//重新开始
				newgame_all();
			}
		}
		//此处可以增加游戏复盘，游戏存档与恢复，游戏界面变换，游戏风格变化等功能
	}
	//屏幕实时显示步数，暂未完全实现
		// 输出数值flag，先将数字格式化输出为字符串（项目字符集）
		//TCHAR s[5];
		//_stprintf(s, _T("步数:%d"), flag);
		//rectangle(630, 550, 790, 600);
		//outtextxy(650,550, s);
}

/*每下一步棋子，判断棋子在4个方向上是否可以练成五子*/
int judge(int a, int b){//判断输赢
	int i, j;
	int t = 2 - flag % 2;
	for (i = a - 4, j = b; i <= a; i++)
	{
		if (i >= 1 && i < 16 && t == board_now[i][j] && t == board_now[i + 1][j] && t == board_now[i + 2][j]
			&& t == board_now[i + 3][j] && t == board_now[i + 4][j])
			return 1;
	}
	for (i = a, j = b - 4; j <= b; j++)
	{
		if (j >= 1 && j < 16 && t == board_now[i][j] && t == board_now[i][j + 1]
			&& t == board_now[i][j + 2] && t == board_now[i][j + 3] && t == board_now[i][j + 4])
			return 1;
	}
	for (i = a - 4, j = b - 4; i <= a, j <= b; i++, j++)
	{
		if (i >= 1 && i < 16 && j >= 1 && j < 16 && t == board_now[i][j] && t == board_now[i + 1][j + 1] &&
			t == board_now[i + 2][j + 2] && t == board_now[i + 3][j + 3] && t == board_now[i + 4][j + 4])
			return 1;
	}
	for (i = a - 4, j = b + 4; i <= a, j >= b; i++, j--)
	{
		if (i >= 1 && i < 16 && j >= 1 && j < 16 && t == board_now[i][j] && t == board_now[i + 1][j - 1] &&
			t == board_now[i + 2][j - 2] && t == board_now[i + 3][j - 3] && t == board_now[i + 4][j - 4])
			return 1;
	}
	return 0;
}

/*重新加载游戏画面和初始化数据，实现认输游戏，重新开始游戏*/
int defeat_black() {
	MessageBox(NULL, "黑棋认输，白棋胜利", "游戏结束", MB_OK);
	newgame_all();
	return 0;
}
int defeat_white(){
	MessageBox(NULL, "白棋认输，黑棋胜利", "游戏结束", MB_OK);
	newgame_all();
	return 0;
}